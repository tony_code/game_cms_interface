package com.game.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * SpringBoot 启动类
 *
 *
 * @since 2019-9-22 17:54:26
 *
 */
@EnableAutoConfiguration
@SpringBootApplication
public class GameCmsApi {

    public static void main( String[] args ) {
        SpringApplication.run(GameCmsApi.class, args);
    }

}
