package com.game.api.jedis;

import java.util.HashMap;
import java.util.Map;

public class JedisService {
    //单例
    public static final JedisService OBJ = new JedisService();

    private Map<String, AbstractJedisFactory> _factoryMap = new HashMap<>();

    /**
     * 注册工厂
     * @param factory
     */
    public void regFactory(AbstractJedisFactory factory){
        _factoryMap.put(factory.jedisConfig.getGameId(), factory);
    }

    /**
     * 获取JedisFactory
     * @param gameId
     * @return
     */
    public AbstractJedisFactory getFactory(String gameId){
        if (_factoryMap.containsKey(gameId)){
            return _factoryMap.get(gameId);
        }
        return null;
    }

}
