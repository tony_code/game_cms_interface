package com.game.api.jedis.impl;

import com.game.api.jedis.AbstractJedisFactory;
import com.game.api.jedis.JedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;

import java.util.HashSet;
import java.util.Set;


@Slf4j
public class StoredManager{

    private static final String charset = "UTF-8";

    public static double incrByFloat(String gameId, String key, double incrment){
        if (StringUtils.isEmpty(key)){
            log.error("key is null");
            return 0;
        }
        AbstractJedisFactory factory = JedisService.OBJ.getFactory(gameId);
        if (factory == null){
            log.error("游戏{}的数据库连接池没找到,没有初始化或者注册",gameId);
            return 0;
        }

        Jedis redis = factory.getJedis();
        try {
            return redis.incrByFloat(key.getBytes(charset), incrment);//对存储在指定key的数值执行原子的加1操作。
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0;
        } finally {
            if (redis != null) {
                redis.close();
            }
        }
    }

    public static String get(String gameId, String key){
        if (StringUtils.isEmpty(key)){
            log.error("key is null");
            return null;
        }

        AbstractJedisFactory factory = JedisService.OBJ.getFactory(gameId);
        if (factory == null){
            log.error("游戏{}的数据库连接池没找到,没有初始化或者注册",gameId);
            return null;
        }

        Jedis redis = factory.getJedis();
        try {
            byte[] bytes = redis.get(key.getBytes(charset));
            if(bytes == null || bytes.length == 0){
                return null;
            }

            return new String(bytes,charset);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        } finally {
            if (redis != null) {
                redis.close();
            }
        }
    }

    public static void set(String gameId, String key, String value){
        if(StringUtils.isEmpty(key)){
            log.error("key is null");
            return;
        }
        if(StringUtils.isEmpty(value)){
            log.error("value is null");
            return;
        }

        AbstractJedisFactory factory = JedisService.OBJ.getFactory(gameId);
        if (factory == null){
            log.error("游戏{}的数据库连接池没找到,没有初始化或者注册",gameId);
            return ;
        }

        Jedis redis = factory.getJedis();
        try {
            redis.set(key.getBytes(charset), value.getBytes(charset));
        } catch (Exception e) {
            log.error("redis setex error:", e);
        }finally{
            redis.close();
        }
    }

    public static Set<String> keys(String gameId, String pattern){
        Set<String> set = new HashSet<>();
        if(StringUtils.isEmpty(pattern)){
            log.error("pattern is null");
            return null;
        }

        AbstractJedisFactory factory = JedisService.OBJ.getFactory(gameId);
        if (factory == null){
            log.error("游戏{}的数据库连接池没找到,没有初始化或者注册",gameId);
            return null;
        }

        pattern += "*";

        Jedis redis = factory.getJedis();
        try {
            Set<byte[]> tmp = redis.keys(pattern.getBytes(charset));
            for (byte[] key : tmp) {
                set.add(new String(key, charset));
            }

            if (set.size() == 0) return null;

            return set;
        } catch (Exception e) {
            log.error("redis setex error:", e);
        }finally{
            redis.close();
        }
        return null;
    }

    public static boolean exsitKey(String gameId, String key){
        if(StringUtils.isEmpty(key)){
            log.error("key is null");
            return false;
        }

        AbstractJedisFactory factory = JedisService.OBJ.getFactory(gameId);
        if (factory == null){
            log.error("游戏{}的数据库连接池没找到,没有初始化或者注册",gameId);
            return false;
        }

        Jedis redis = factory.getJedis();
        try {
            return redis.exists(key.getBytes(charset));
        } catch (Exception e) {
            log.error("redis setex error:", e);
        }finally{
            redis.close();
        }
        return false;
    }

    public static boolean del(String gameId, String key){
        if(StringUtils.isEmpty(key)){
            log.error("key is null");
            return false;
        }

        AbstractJedisFactory factory = JedisService.OBJ.getFactory(gameId);
        if (factory == null){
            log.error("游戏{}的数据库连接池没找到,没有初始化或者注册",gameId);
            return false;
        }

        Jedis redis = factory.getJedis();
        try {
            long result = redis.del(key.getBytes(charset));
            if (result == 1){
                return true;
            }
        } catch (Exception e) {
            log.error("redis setex error:", e);
        }finally{
            redis.close();
        }
        return false;
    }
}
