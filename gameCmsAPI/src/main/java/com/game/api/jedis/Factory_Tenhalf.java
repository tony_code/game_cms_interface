package com.game.api.jedis;

import com.game.api.config.JedisConfigTenhalf;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Slf4j
public class Factory_Tenhalf extends AbstractJedisFactory<JedisConfigTenhalf> {

    @Override
    public void init(JedisConfigTenhalf config) {
        jedisConfig = config;
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(config.getPoolMaxTotal());
        jedisPoolConfig.setMaxIdle(config.getPoolMaxIdle());

        JedisPool jedisPool = new JedisPool(jedisPoolConfig, config.getHost(), config.getPort(), 0, config.getPassword());
        jedisConfig.setPool(jedisPool);
        jedisConfig.setIndex(config.getDbIndex());
        jedisConfig.setGameId(config.getServertype());
    }

}
