package com.game.api.jedis;

import com.game.api.config.JedisConfigNiuniu;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Slf4j
public class Factory_Niuniu extends AbstractJedisFactory<JedisConfigNiuniu> {

    @Override
    public void init(JedisConfigNiuniu config) {
        jedisConfig = config;
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(config.getPoolMaxTotal());
        jedisPoolConfig.setMaxIdle(config.getPoolMaxIdle());

        JedisPool jedisPool = new JedisPool(jedisPoolConfig, config.getHost(), config.getPort(), 0, config.getPassword());
        jedisConfig.setPool(jedisPool);
        jedisConfig.setIndex(config.getDbIndex());
        jedisConfig.setGameId(config.getServertype());
    }

}
