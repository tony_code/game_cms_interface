package com.game.api.jedis;

import com.game.api.config.AbstractJedisConfig;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

@Slf4j
public abstract class AbstractJedisFactory<T extends AbstractJedisConfig> {

    protected AbstractJedisConfig jedisConfig = null;

    public abstract void init(T config);

    /**
     * 获取连接池
     * @return
     */
    public Jedis getJedis() {
        Jedis jedis = jedisConfig.getPool().getResource();
        if (jedis == null) {
            log.error("Error accessing Jedis ");
        }
        jedis.select(jedisConfig.getIndex());
        return jedis;
    }

    /**
     * 测试连接池
     */
    public void isConnected(){
        try {
            Jedis jedis = this.getJedis();
            try {
                boolean connected = jedis.isConnected();
                if (connected){
                    log.info("连接池测试,已经连接成功.");
                }else {
                    log.error("连接池测试,未连接成功.");
                }
            }catch (Exception ex){
                log.error("连接池异常", ex.getMessage());
            } finally {
                jedis.close();
            }
        }catch (Exception e){
            log.error("连接池测试,异常连接", e.getMessage());
        }
    }

}
