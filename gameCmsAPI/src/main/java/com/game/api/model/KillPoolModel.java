package com.game.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter@Getter
public class KillPoolModel {

    //渠道
    private String channelId;

    //房间 1初级 2中级 3高级 4至尊
    private String roomId;

    //Redis键
    private String xKey;

    //本月奖池
    private String xValue;

    private String zKey;

    private String zValue;

    public KillPoolModel(String channelId, int roomId, String xKey, String xValue, String zKey, String zValue) {
        this.channelId = channelId;
        this.roomId = String.valueOf(roomId);
        this.xKey = xKey;
        this.xValue = xValue;
        this.zKey = zKey;
        this.zValue = zValue;
    }
}
