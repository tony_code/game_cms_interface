package com.game.api.model;

import com.game.api.utils.GsonUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter@Slf4j
public class ReMsg {

    //返回code 0正常，其他异常
    private int code = 0;

    //返回异常信息
    private String msg;

    //返回内容
    private String content;


    public ReMsg setCode(int code) {
        this.code = code;
        return this;
    }

    public ReMsg setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public ReMsg setContent(String content) {
        this.content = content;
        return this;
    }

    @Override
    public String toString() {
        String ret = GsonUtils.gson.toJson(this);
        log.info("请求返回结果 ret = {}", ret);
        return ret;
    }
}
