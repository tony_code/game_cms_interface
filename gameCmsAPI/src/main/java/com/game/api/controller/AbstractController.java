package com.game.api.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * controller父类
 *
 * @author Tony
 *
 * @since 2019-9-22 17:57:33
 *
 */
public abstract class AbstractController {

    protected static final List<Integer> rooms = Arrays.asList(1,2,3,4);

    //log4j日志
    protected static final Logger log = LoggerFactory.getLogger(AbstractController.class);

    //gson
    protected static final Gson gson = new GsonBuilder().serializeNulls().create();

    protected static final String split = "_";

    /**异常定义**/
    protected static final String success = "操作成功";

    protected static final String param_error = "参数异常";

    protected static final String result_empty = "结果为空";

}
