package com.game.api.controller.goldfast;

import com.game.api.jedis.impl.StoredManager;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *  cms-游戏 API、
 *
 *  急速金花
 *
 *  游戏维护
 */
@RestController(value = "maintent_goldfast")
@RequestMapping(value = "/api/server/")
public class Controller_Maintent extends Controller {

    //redis 里的维护标识
    private static final String maintent_flag = "Game_Maintent_Flag_"+ gameId;

    @RequestMapping(value = gameId+"/state", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public int getMaintentState(){
        return StoredManager.exsitKey(gameId, maintent_flag) ? 1 : 0;
    }

    @RequestMapping(value = gameId+"/shift", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public int setMaintentState(){
        if (StoredManager.exsitKey(gameId, maintent_flag)){
            StoredManager.del(gameId, maintent_flag);
        }else {
            StoredManager.set(gameId, maintent_flag, "1");
        }
        return 1;
    }

}
