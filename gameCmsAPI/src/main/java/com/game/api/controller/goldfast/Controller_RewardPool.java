package com.game.api.controller.goldfast;


import com.game.api.jedis.impl.StoredManager;
import com.game.api.model.KillPoolModel;
import com.game.api.model.ReMsg;
import com.game.api.utils.TimeUitls;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * cms-游戏 API
 *
 * 急速扎金花
 *
 * 奖池查询修改
 */
@RestController(value = "reward_pool_goldfast")
@RequestMapping(value = "/api/data/")
public class Controller_RewardPool extends Controller {

    private static final String x_key_prefix = "ROBOT_POOL_CURRENT_MONEY_KEY_1014";
    private static final String z_key_prefix = "GAME_KILL_AMOUNT_POOL_KEY_1014";


    @RequestMapping(value = gameId+"/query", method={RequestMethod.GET, RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String queryRewardPool(@RequestParam("channelId") String channelId) {
        log.info("查询渠道奖池接口query调用,参数channelId={}",channelId);

        ReMsg msg = new ReMsg();
        if (StringUtils.isEmpty(channelId)){
            return msg.setCode(1).setMsg(param_error).toString();
        }

        List<KillPoolModel> list = new ArrayList<>();
        //键的拼接
        String date = TimeUitls.formatDate(new Date());
        for(Integer roomId : rooms){
            //拼接对应房间键值
            String x_key = x_key_prefix + date + split + channelId + split + roomId;
            String z_key = z_key_prefix + date + split + channelId + split + roomId;

            //查询结果
            String x_value = StoredManager.get(gameId, x_key);
            String z_value = StoredManager.get(gameId, z_key);

            KillPoolModel poolModel = new KillPoolModel(channelId, roomId, x_key, x_value, z_key, z_value);
            list.add(poolModel);
        }

        if (list.isEmpty()){
            return msg.setCode(2).setMsg(result_empty).toString();
        }

        return msg.setMsg(success).setContent(gson.toJson(list)).toString();
    }

    @RequestMapping(value = gameId+"/change", method={RequestMethod.GET, RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String changeRewardPool(@RequestParam("channelId") String channelId,
                                   @RequestParam("key") String key,
                                   @RequestParam("value") String value) {
        log.info("修改渠道奖池接口change调用,参数channelId={},key={},value={}",channelId, key, value);
        ReMsg msg = new ReMsg();

        if (StringUtils.isEmpty(channelId) || StringUtils.isEmpty(key) || StringUtils.isEmpty(value)){
            return msg.setCode(1).setMsg(param_error).toString();
        }

        //key 是否存在
        if (!StoredManager.exsitKey(gameId, key)){
            log.error("修改渠道奖池接口change调用, key = {} 不存在",key);
            return msg.setCode(1).setMsg(param_error).toString();
        }

        //当前值
        String curValue = StoredManager.get(gameId, key);
        StoredManager.set(gameId, key, value);

        log.info("修改渠道奖池接口change调用,设置key = {} 的值,修改前value={},修改后value={}", key, curValue, value);

        return msg.setMsg(success).toString();
    }

}
