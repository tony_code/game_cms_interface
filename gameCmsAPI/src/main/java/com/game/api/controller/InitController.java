package com.game.api.controller;

import com.game.api.config.*;
import com.game.api.jedis.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.PostConstruct;



@RestController
public class InitController extends AbstractController{

    @Autowired
    private JedisConfigGold cfg_gold;
    @Autowired
    private JedisConfigDomino cfg_domino;
    @Autowired
    private JedisConfigGoldFast cfg_goldfast;
    @Autowired
    private JedisConfigFacecard cfg_facecard;
    @Autowired
    private JedisConfigNiuniuOpen cfg_niuniuopen;
    @Autowired
    private JedisConfigNiuniu cfg_niuniu;
    @Autowired
    private JedisConfigTenhalf cfg_tenhalf;
    @Autowired
    private JedisConfigThirteen cfg_thirteen;


    @PostConstruct
    public void init(){
        //初始化启动项
        initJedisPool();
    }


    //初始化Jedi连接池
    private void initJedisPool(){
        initPoolGold();
        initPoolGoldfast();
        initPoolDomino();
        initPoolFacecard();
        initPoolNiuniu4();
        initPoolNiuniu();
        initPoolTenhalf();
        initPoolThirteen();
    }

    //扎金花Jedis初始化
    private void initPoolGold(){
        log.info("初始化扎金花Jedis连接池 cfg = {}",cfg_gold.toString());
        AbstractJedisFactory factory = new Factory_Gold();
        factory.init(cfg_gold);
        factory.isConnected();
        JedisService.OBJ.regFactory(factory);
    }

    private void initPoolGoldfast(){
        log.info("初始化急速扎金花Jedis连接池 cfg = {}",cfg_goldfast.toString());
        AbstractJedisFactory factory = new Factory_Goldfast();
        factory.init(cfg_goldfast);
        factory.isConnected();
        JedisService.OBJ.regFactory(factory);
    }

    private void initPoolDomino(){
        log.info("初始化牌九Jedis连接池 cfg = {}",cfg_domino.toString());
        AbstractJedisFactory factory = new Factory_Domino();
        factory.init(cfg_domino);
        factory.isConnected();
        JedisService.OBJ.regFactory(factory);
    }

    private void initPoolFacecard(){
        log.info("初始化三公Jedis连接池 cfg = {}",cfg_facecard.toString());
        AbstractJedisFactory factory = new Factory_Facecard();
        factory.init(cfg_facecard);
        factory.isConnected();
        JedisService.OBJ.regFactory(factory);
    }

    private void initPoolNiuniu4(){
        log.info("初始化4张牛牛Jedis连接池 cfg = {}",cfg_niuniuopen.toString());
        AbstractJedisFactory factory = new Factory_Niuniu4();
        factory.init(cfg_niuniuopen);
        factory.isConnected();
        JedisService.OBJ.regFactory(factory);
    }

    private void initPoolNiuniu(){
        log.info("初始化牛牛Jedis连接池 cfg = {}",cfg_niuniu.toString());
        AbstractJedisFactory factory = new Factory_Niuniu();
        factory.init(cfg_niuniu);
        factory.isConnected();
        JedisService.OBJ.regFactory(factory);
    }

    private void initPoolTenhalf(){
        log.info("初始化十点半Jedis连接池 cfg = {}",cfg_tenhalf.toString());
        AbstractJedisFactory factory = new Factory_Tenhalf();
        factory.init(cfg_tenhalf);
        factory.isConnected();
        JedisService.OBJ.regFactory(factory);
    }

    private void initPoolThirteen(){
        log.info("初始化十三水Jedis连接池 cfg = {}",cfg_thirteen.toString());
        AbstractJedisFactory factory = new Factory_Thirteen();
        factory.init(cfg_thirteen);
        factory.isConnected();
        JedisService.OBJ.regFactory(factory);
    }
}
