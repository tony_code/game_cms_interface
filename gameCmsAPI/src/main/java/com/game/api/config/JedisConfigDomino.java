package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@ToString@Getter@Setter
@Component
public class JedisConfigDomino extends AbstractJedisConfig{
    @Value("${redis.domino.ip}")
    private String host;
    @Value("${redis.domino.port}")
    private int port;
    @Value("${redis.common.max_total}")
    private int poolMaxTotal;
    @Value("${redis.common.max_idle}")
    private int poolMaxIdle;
    @Value("${redis.domino.db_id}")
    private int dbIndex;
    @Value("${redis.domino.password}")
    private String password;
    @Value("${redis.domino.servertype}")
    private String servertype;
}
