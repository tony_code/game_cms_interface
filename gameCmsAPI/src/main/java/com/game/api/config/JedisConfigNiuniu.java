package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@ToString@Getter@Setter
@Component
public class JedisConfigNiuniu extends AbstractJedisConfig{
    @Value("${redis.niuniu.ip}")
    private String host;
    @Value("${redis.niuniu.port}")
    private int port;
    @Value("${redis.common.max_total}")
    private int poolMaxTotal;
    @Value("${redis.common.max_idle}")
    private int poolMaxIdle;
    @Value("${redis.niuniu.db_id}")
    private int dbIndex;
    @Value("${redis.niuniu.password}")
    private String password;
    @Value("${redis.niuniu.servertype}")
    private String servertype;
}
