package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@ToString@Getter@Setter
@Component
public class JedisConfigNiuniuOpen extends AbstractJedisConfig{
    @Value("${redis.niuniu4.ip}")
    private String host;
    @Value("${redis.niuniu4.port}")
    private int port;
    @Value("${redis.common.max_total}")
    private int poolMaxTotal;
    @Value("${redis.common.max_idle}")
    private int poolMaxIdle;
    @Value("${redis.niuniu4.db_id}")
    private int dbIndex;
    @Value("${redis.niuniu4.password}")
    private String password;
    @Value("${redis.niuniu4.servertype}")
    private String servertype;
}
