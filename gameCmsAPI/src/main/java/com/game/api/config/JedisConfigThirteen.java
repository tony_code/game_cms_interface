package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@ToString@Getter@Setter
@Component
public class JedisConfigThirteen extends AbstractJedisConfig{
    @Value("${redis.thirteen.ip}")
    private String host;
    @Value("${redis.thirteen.port}")
    private int port;
    @Value("${redis.common.max_total}")
    private int poolMaxTotal;
    @Value("${redis.common.max_idle}")
    private int poolMaxIdle;
    @Value("${redis.thirteen.db_id}")
    private int dbIndex;
    @Value("${redis.thirteen.password}")
    private String password;
    @Value("${redis.thirteen.servertype}")
    private String servertype;
}
