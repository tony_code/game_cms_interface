package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@ToString@Getter@Setter
@Component
public class JedisConfigFacecard extends AbstractJedisConfig{
    @Value("${redis.facecard.ip}")
    private String host;
    @Value("${redis.facecard.port}")
    private int port;
    @Value("${redis.common.max_total}")
    private int poolMaxTotal;
    @Value("${redis.common.max_idle}")
    private int poolMaxIdle;
    @Value("${redis.facecard.db_id}")
    private int dbIndex;
    @Value("${redis.facecard.password}")
    private String password;
    @Value("${redis.facecard.servertype}")
    private String servertype;
}
