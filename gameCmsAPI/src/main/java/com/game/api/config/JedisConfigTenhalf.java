package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@ToString@Getter@Setter
@Component
public class JedisConfigTenhalf extends AbstractJedisConfig{
    @Value("${redis.tenhalf.ip}")
    private String host;
    @Value("${redis.tenhalf.port}")
    private int port;
    @Value("${redis.common.max_total}")
    private int poolMaxTotal;
    @Value("${redis.common.max_idle}")
    private int poolMaxIdle;
    @Value("${redis.tenhalf.db_id}")
    private int dbIndex;
    @Value("${redis.tenhalf.password}")
    private String password;
    @Value("${redis.tenhalf.servertype}")
    private String servertype;
}
