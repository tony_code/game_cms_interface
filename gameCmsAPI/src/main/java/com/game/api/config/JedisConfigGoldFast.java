package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@ToString@Getter@Setter
@Component
public class JedisConfigGoldFast extends AbstractJedisConfig{
    @Value("${redis.goldfast.ip}")
    private String host;
    @Value("${redis.goldfast.port}")
    private int port;
    @Value("${redis.common.max_total}")
    private int poolMaxTotal;
    @Value("${redis.common.max_idle}")
    private int poolMaxIdle;
    @Value("${redis.goldfast.db_id}")
    private int dbIndex;
    @Value("${redis.goldfast.password}")
    private String password;
    @Value("${redis.goldfast.servertype}")
    private String servertype;
}
