package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import redis.clients.jedis.JedisPool;

@Getter@Setter
public abstract class AbstractJedisConfig {

    //连接池
    protected JedisPool pool;

    protected int index;

    protected String gameId;

}
