package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@ToString@Getter@Setter
@Component
public class JedisConfigGold extends AbstractJedisConfig{
    @Value("${redis.gold.ip}")
    private String host;
    @Value("${redis.gold.port}")
    private int port;
    @Value("${redis.common.max_total}")
    private int poolMaxTotal;
    @Value("${redis.common.max_idle}")
    private int poolMaxIdle;
    @Value("${redis.gold.db_id}")
    private int dbIndex;
    @Value("${redis.gold.password}")
    private String password;
    @Value("${redis.gold.servertype}")
    private String servertype;
}
