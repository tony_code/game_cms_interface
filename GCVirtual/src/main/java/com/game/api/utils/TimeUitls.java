package com.game.api.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUitls {

    /**
     * 时间格式化 yyyy-MM
     * @return
     */
    public static String formatDate(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMM");
        return formatter.format(date);
    }

}
