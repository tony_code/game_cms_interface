package com.game.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@ToString@Getter@Setter
@Component
public class Config {
    @Value("${game.control}")
    private int control;

    @Value("${game.alllose}")
    private String alllose;

    @Value("${game.allwin}")
    private String allwin;

    @Value("${game.natural}")
    private String natural;
}
