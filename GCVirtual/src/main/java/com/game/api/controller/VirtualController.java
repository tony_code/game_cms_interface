package com.game.api.controller;


import com.game.api.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VirtualController {

    @Autowired
    private Config config;

    @RequestMapping(value = "/c_req", method= {RequestMethod.GET,RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String req(){
        if (config.getControl() == 1){
            return config.getNatural();
        }

        if (config.getControl() == 2){
            //2-控真人输
            return config.getAlllose();
        }
        return config.getAllwin();
    }

    @RequestMapping(value = "/c_update", method= {RequestMethod.GET,RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String update(){
        return "{\"code\":0,\"msg\":\"success\"}";
    }
}
